@extends('layouts.app_public')

@section('content')
<div class="home-page">
    <div class="container-large">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators"> 
                @php
                $i=0;
                @endphp 
                @foreach($carousel as $slide)
                <li data-target="#myCarousel" data-slide-to="{{$i}}" class="{{ $i == 0 ? 'active' : ''}}"></li>
                @php
                $i++;
                @endphp 
                @endforeach
            </ol>
            <div class="carousel-inner">
                @php
                $i=0;
                @endphp 
                @foreach($carousel as $slide)
                <div class="item {{ $i == 0 ? 'active' : ''}}">
                    <a href="{{$slide->link}}">
                        <img src="{{asset('storage/'.$slide->image)}}" alt="">
                    </a>
                </div>
                @php
                $i++;
                @endphp 
                @endforeach
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    
    <div class="container">
        <div class="row featured">
            <!-- <div class="col-xs-12 section-title">
                <h2>{{__('public_pages.featured_category')}}</h2>
            </div> -->
            <div id="inner-slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    @php
                    $i=0;
                    @endphp 
                    @php
                    if (!empty($featuredProducts)) {
                        foreach ($featuredProducts as $featuredProduct) {
                            @endphp
                                <div class="{{ $i == 0 ? 'item active' : 'item'}}">
                                    <div class="img-container col-lg-2">
                                        <h2 class="d-inherit">{{$featuredProduct->name}}</h2>
                                        <!-- <span class="d-inherit mb-8">{{$featuredProduct->description}}</span> -->
                                        <img class="center-block" src="{{asset('storage/'.$featuredProduct->image)}}" data-num="{{$i}}" alt="{{$featuredProduct->name}}">
                                    </div>
                                </div>
                            @php
                            $i++;
                        }
                    }
                    @endphp
                </div>
                <div class="controls">
                    <a class="left carousel-control" href="#inner-slider" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </a>
                    <a class="right carousel-control" href="#inner-slider" role="button" data-slide="next">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-xs-12 section-title">
                <h2>{{__('public_pages.products')}}</h2>
            </div> -->
            @foreach ($mostSelledProducts as $mostSelledProduct)
            <div class="col-xs-6 col-sm-4 col-md-3 product-container">
                 <div class="product">
                    <div class="img-container">
                        <a href="{{ lang_url($mostSelledProduct->url) }}">
                            <img src="{{asset('storage/'.$mostSelledProduct->image)}}" alt="{{$mostSelledProduct->name}}">
                        </a>
                    </div>
                    <a href="{{ lang_url($mostSelledProduct->url) }}">
                        <h1>{{$mostSelledProduct->name}}</h1>
                    </a>
                    <span class="price">{{$mostSelledProduct->price}}</span>
                    @php
                    if($mostSelledProduct->link_to != null) {
                    @endphp
                    <a href="{{lang_url($mostSelledProduct->url)}}" class="buy-now" title="{{$mostSelledProduct->name}}">{{__('public_pages.buy')}}</a>
                    @php
                    } else {
                    @endphp
                    <!-- <a href="javascript:void(0);" data-product-id="{{$mostSelledProduct->id}}" class="buy-now to-cart">{{__('public_pages.buy')}}</a> -->
                    @php
                    }
                    @endphp
                </div>
            </div>
            @endforeach
        </div> 
    </div>
</div>

<script>
$('#inner-slider').carousel({
  interval: 40000
})

$('.featured .carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<0;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});

</script>
@endsection
